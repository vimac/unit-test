function sum (a, b){
    return a+b;
}
module.exports = sum;


// function errorFormatCSVFile(){
//     throw new Error ('the format is incorrect');
// }

// function compileAndroidCode() {
//     throw new Error('you are using the wrong JDK');
//   }
// module.exports = compileAndroidCode;
  
expect.extend({
  toBeWithinRange(received, floor, ceiling) {
    const pass = received >= floor && received <= ceiling;
    if (pass) {
      return {
        message: () =>
          `expected ${received} not to be within range ${floor} - ${ceiling}`,
        pass: true,
      };
    } else {
      return {
        message: () =>
          `expected ${received} to be within range ${floor} - ${ceiling}`,
        pass: false,
      };
    }
  },
});