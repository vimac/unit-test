  const hasEnoughPremiumContent = ({
    stats,
    collection,
    minPremiumContent = 10000, // Following logic on #playlist
    minSubcribeContent = -1, // Following logic on #playlist
  }) => {
    let subscribeCount = 0;
    let customCount = 0;
  
    if (collection.indexOf("subscribe") > -1 && stats.subscribe) {
      subscribeCount = stats.subscribe;
    }
  
    if (collection.indexOf("custom") > -1 && stats.custom) {
      customCount = stats.custom;
    }
  
    const hasEnoughContent = subscribeCount + customCount > minPremiumContent;
  
    const hasEnoughSubscription =
      collection.indexOf("subscribe") > -1 && stats.subscribe
        ? stats.subscribe > minSubcribeContent
        : false;
  
    return hasEnoughContent && hasEnoughSubscription;
  };
  module.exports = hasEnoughPremiumContent;
  