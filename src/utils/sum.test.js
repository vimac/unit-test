const sum = require ('./sum');
test('adds 1 + 2 to equal 3',()=>{
    expect(sum(1,2)).toBe(3);
});

test('two plus two is four',()=>{
    expect(sum(2,2)).toBe(4);
});

test('object assigment',()=>{
    const data = {
        one: 1,
        two: 2
    };
    expect(data.one).toEqual(1);
    expect(data.two).toEqual(2);
});

test('adding positive numbers is not zero',()=>{
    for(let a = 1; a<10; a++){
        for(let b =1; b <10; b++){
            expect(a+b).not.toBe(0);
        }
    }
})

test('null',()=>{
    const n = null;
    expect(n).toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).not.toBeTruthy();
    expect(n).toBeFalsy();
});

test('true/false',()=>{
    const m = true;
    expect(m).not.toBeNull();
    expect(m).toBeDefined();
    expect(m).not.toBeUndefined();
    expect(m).toBeTruthy();
    expect(m).not.toBeFalsy();
});

test('zero',()=>{
    const xx = 0;
    expect(xx).not.toBeNull();
    expect(xx).toBeDefined();
    expect(xx).not.toBeUndefined();
    expect(xx).not.toBeTruthy();
    expect(xx).toBeFalsy();
});

test('number: two plus two equal 4',()=>{
    const value = 2+2;
    expect(value).toBeGreaterThan(3);
    expect(value).toBeGreaterThanOrEqual(3.99);
    expect(value).toBeLessThan(4.1);
    expect(value).toBeLessThanOrEqual(4.01);
    expect(value).toBe(4);
    expect(value).toEqual(4);
})

test('floating: 0.1 + 0.3 close to 0.4',()=>{
    const f = 0.1 + 0.3;
    expect(f).toBeCloseTo(0.4);
    expect(f).toEqual(0.4);
})

const can1 = {
    flavor: 'grapefruit',
    ounces: 12,
  };
  const can2 = {
    flavor: 'grapefruit',
    ounces: 12,
  };
  
  describe('the La Croix cans on my desk', () => {
    test('have all the same properties', () => {
      expect(can1).toEqual(can2);
    });
    test('are not the exact same can', () => {
      expect(can1).not.toBe(can2);
    });
  });

  describe('grapefruits are healthy', () => {
    test('grapefruits are a fruit', () => {
      expect('are').toMatch('are');
    });
  });

  test('there is no I in team', () => {
    expect('team').not.toMatch(/is/);
  });
  
  test('but there is a "stop" in Christoph', () => {
    expect('Christoph').toMatch(/stop/);
  });

  const shoppingList = [
      'milk',
      'water',
      'organce',
      'carrot',
      'kale'
  ];
  test('the shopping list has kale',()=>{
      expect(shoppingList).toContain('kale');
  })

//   describe('expect exception',()=>{
//       test('throw error',()=>{
//           expect(()=> errorFormatCSVFile()).toThrow();
//           expect(()=>errorFormatCSVFile()).toThrow(Error);
//           expect(()=>errorFormatCSVFile()).toThrow('the format is incorrect');
//           expect(()=>errorFormatCSVFile()).toThrow(/format/);
//       })
//   })

// function compileAndroidCode() {
//     throw new Error('you are using the wrong JDK');
//   }
  
  // test('compiling android goes as expected', () => {
  //   expect(() => compileAndroidCode()).toThrow();
  //   expect(() => compileAndroidCode()).toThrow(Error);
  
  //   // You can also use the exact error message or a regexp
  //   expect(() => compileAndroidCode()).toThrow('you are using the wrong JDK');
  //   expect(() => compileAndroidCode()).toThrow(/JDK/);
  // });

test('numeric ranges', () => {
  expect(100).toBeWithinRange(90, 110);
  expect(101).not.toBeWithinRange(0, 100);
  expect({apples: 6, bananas: 3}).toEqual({
    apples: expect.toBeWithinRange(1, 10),
    bananas: expect.not.toBeWithinRange(11, 20),
  });
});

describe('arrayContainning',()=>{
  const expected = ['water','cocal','sprite'];
  test('matches even if received contains additional elements',()=>{
    expect(['water','cocal','sprite','pepsi']).toEqual(expect.arrayContaining(expected));
  });

  test('does not match if received does not contain expected elements',()=>{
    expect(['cocal','sprite','pepsi']).not.toEqual(expect.arrayContaining(expected));
  });
});

describe('not.arrayContaining',()=>{
  const expectedMotobike = ['Vinfast', 'Honda', 'Toyota'];
  it('matches if the actual array does not contain the expected elements',()=>{
    expect(['Vinfast', 'Honda', 'Kia']).toEqual(expect.not.arrayContaining(expectedMotobike));
  });
});

describe('not.objectContaining',()=>{
  const expected = {name: 'Vi'};
  it('matches if the actual object does not contain expected key: value pairs',()=>{
    expect({name:'Tinh'}).toEqual(expect.not.objectContaining(expected));
  });
});

describe('not.stringMatching', () => {
  const expected = /Hello world!/;

  it('matches if the received value does not match the expected regex', () => {
    expect('Hello world! Vi').toEqual(expect.stringMatching(expected));
  });
});