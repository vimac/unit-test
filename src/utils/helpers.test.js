const hasEnoughPremiumContent = require ('./helpers');

describe('Checking: has enough premium content in a portal ',()=>{
    test('5k + 5k => not correct',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 5000, custom: 5000},
            collection:["subscribe", "custom"]
        })).not.toBeTruthy();
    });

    test('9999 + 1 => not correct',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 9999, custom: 1},
            collection:["subscribe", "custom"]
        })).not.toBeTruthy();
    });

    test('1 + 9999 => correct',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 1, custom: 9999},
            collection:["subscribe", "custom"]
        })).toBeFalsy();
    });

    test('Custom = 0 => correct',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 10001, custom: 0},
            collection:["subscribe", "custom"]
        })).toBeTruthy();
    });

    test('Subscribe = 0 => incorrect',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 0, custom: 10001},
            collection:["subscribe", "custom"]
        })).toBeFalsy();
    });

    test('Subscribe = 0 => correct',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: -1, custom: 10002},
            collection:["subscribe", "custom"]
        })).not.toBeTruthy();
    });

    test('Array have to contain subscribe',()=>{
        expect(hasEnoughPremiumContent({
            stats: {subscribe: 84662, custom: 6},
            collection:['custom']
        })).toBeFalsy();
    });

    test('Check minimum subcribe content',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 5001, custom: 5001},
            collection:["subscribe", "custom_share"],
        })).not.toBeTruthy();
    });

    test('Checking collection',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 5001, custom: 5001},
            collection:[""],
        })).not.toBeTruthy();
    });

    test('Checking object stats',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 10000},
            collection:["subscribe", "custom"],
        })).not.toBeTruthy();
    });

    test('Check premium portal',()=>{
        expect(hasEnoughPremiumContent({
            stats:{subscribe: 400, custom: 400},
            collection:["subscribe", "custom"],
            minPremiumContent: 700,
            minSubcribeContent: 300
        })).toBeTruthy();
    });
})